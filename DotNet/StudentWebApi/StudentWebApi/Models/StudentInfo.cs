﻿using System;
using System.Collections.Generic;

namespace StudentWebApi.Models;

public partial class StudentInfo
{
    public int StudentId { get; set; }

    public string? StudentName { get; set; }

    public string? Address { get; set; }

    public int? CourseId { get; set; }

    public virtual Course? Course { get; set; }
}
