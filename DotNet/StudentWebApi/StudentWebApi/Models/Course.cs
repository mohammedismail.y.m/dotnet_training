﻿using System;
using System.Collections.Generic;

namespace StudentWebApi.Models;

public partial class Course
{
    public int CourseId { get; set; }

    public string CourseName { get; set; } = null!;

    public int? Fees { get; set; }

    public virtual ICollection<StudentInfo> StudentInfos { get; } = new List<StudentInfo>();
}
