﻿using StudentWebApi.Interfaces;
using StudentWebApi.Models;

namespace StudentWebApi.Repository
{
    public class StudentRepository:IStudent
    {
        private readonly StudentContext _context;

        public StudentRepository(StudentContext context)
        {
            _context = context;
        }

        public List<StudentInfo> AddNewStudent(StudentInfo student)
        {
            //throw new NotImplementedException();
            _context.StudentInfos.Add(student);
            _context.SaveChanges();
            return _context.StudentInfos.ToList();

        }

        public string DeleteStudent(int id)
        {
           
            var currentStudent = _context.StudentInfos.Find(id);
            _context.StudentInfos.Remove(currentStudent);
            _context.SaveChanges();
            return "Deletd Successfully";
        }

       

        public List<StudentInfo> GetAllStudents()
        {
            return _context.StudentInfos.ToList();
        }

        public List<StudentInfo> GetStudentById(int id)
        {
            throw new NotImplementedException();
        }

        public string updateStudent(StudentInfo student)
        {
            var currentStudent = _context.StudentInfos.Find(student.StudentId);
            if (currentStudent != null)
            {
                currentStudent.StudentName = student.StudentName;
                currentStudent.Address = student.Address;
                currentStudent.CourseId = student.CourseId;
                currentStudent.Course = student.Course;
                _context.SaveChanges();
                return "Updated";
            }

            return "Not found";

            

        }
    }
}
