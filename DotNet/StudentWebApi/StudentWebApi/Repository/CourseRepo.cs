﻿using Microsoft.Data.SqlClient;
using StudentWebApi.Interfaces;
using StudentWebApi.Models;

namespace StudentWebApi.Repository
{
    public class ReferenceKeyException : Exception
    {
        public ReferenceKeyException(String message):base(message) { }
    }
    public class CourseRepo:ICourse
    {
        private readonly StudentContext _contextCourse;

        public CourseRepo(StudentContext contextCourse)
        {
            _contextCourse = contextCourse;
        }

        public List<Course> AddNewCourse(Course course)
        {
            _contextCourse.Courses.Add(course);
            _contextCourse.SaveChanges();
            return _contextCourse.Courses.ToList();
        }

        public string DeleteCourse(int id)
        {
            try
            {
                var currentCourse = _contextCourse.Courses.Find(id);
                if (currentCourse != null)
                {

                    _contextCourse.Courses.Remove(currentCourse);
                    _contextCourse.SaveChanges();
                    return "deleted successfully";

                }
                return "Not Found";

            }
            catch (Exception entityException)
            {

                return entityException.Message;
            }


        }

        public List<Course> getAllCourses()
        {
            return _contextCourse.Courses.ToList();

        }

        public string updateCourse(Course course)
        {
            var currentCourse = _contextCourse.Courses.Find(course.CourseId);
            if (currentCourse != null)
            {
                currentCourse.CourseName = course.CourseName;
                currentCourse.Fees= course.Fees;
                _contextCourse.SaveChanges();
                return "updated successfully";
            }
            return "Not found";
        }
    }
}
