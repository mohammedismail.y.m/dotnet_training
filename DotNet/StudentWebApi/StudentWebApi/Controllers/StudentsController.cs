﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentWebApi.Interfaces;
using StudentWebApi.Models;
using StudentWebApi.Repository;

namespace StudentWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudent _studentRepo;

        public StudentsController(IStudent studentRepo)
        {
            _studentRepo = studentRepo;
        }

        [HttpPost]
        public List<StudentInfo> AddStudent(StudentInfo student)
        {
            
            return _studentRepo.AddNewStudent(student);
        }

        [HttpGet]
        public List<StudentInfo> GetStudents()
        {
            return _studentRepo.GetAllStudents();
        }

        [HttpDelete("{StudentId}")]
        public string DeleteStudent(int id)
        {
            return _studentRepo.DeleteStudent(id);
        }

        [HttpPut]
        public string editStudent(StudentInfo student) {

            return _studentRepo.updateStudent(student);
        }

    }
}
