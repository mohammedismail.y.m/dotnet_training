﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentWebApi.Interfaces;
using StudentWebApi.Models;

namespace StudentWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly ICourse _courseRepo;

        public CourseController(ICourse courseRepo)
        {
            _courseRepo = courseRepo;
        }

        [HttpGet]
        public List<Course> getCourses()
        {
            return _courseRepo.getAllCourses();
        }

        [HttpPost]
        public List<Course> addNewCourse(Course course)
        {
            return _courseRepo.AddNewCourse(course);
        }

        [HttpPut]
        public string updateCourse(Course course)
        {
            return _courseRepo.updateCourse(course);
        }

        [HttpDelete("{CourseId}")]
        public string deleteCourse(int id)
        {
            return _courseRepo.DeleteCourse(id);
        }
    }
}
