﻿using StudentWebApi.Models;

namespace StudentWebApi.Interfaces
{
    public interface IStudent
    {
        List<StudentInfo> AddNewStudent(StudentInfo student);

        List<StudentInfo> GetAllStudents();

        List<StudentInfo> GetStudentById(int id);

        String DeleteStudent(int id);

        String updateStudent(StudentInfo student);
    }
}
