﻿using StudentWebApi.Models;

namespace StudentWebApi.Interfaces
{
    public interface ICourse
    {
        List<Course> getAllCourses();

        List<Course> AddNewCourse(Course course);

        String updateCourse(Course course);

        String DeleteCourse(int id);

    }
}
