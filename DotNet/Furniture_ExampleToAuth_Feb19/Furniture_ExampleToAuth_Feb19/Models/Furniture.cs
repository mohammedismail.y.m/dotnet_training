﻿using System;
using System.Collections.Generic;

namespace Furniture_ExampleToAuth_Feb19.Models;

public partial class Furniture
{
    public int Id { get; set; }

    public string? FurnitureName { get; set; }

    public int? Cost { get; set; }
}
