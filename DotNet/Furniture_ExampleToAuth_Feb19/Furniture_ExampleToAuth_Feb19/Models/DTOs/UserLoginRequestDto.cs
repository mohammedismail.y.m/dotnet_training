﻿using System.ComponentModel.DataAnnotations;

namespace Furniture_ExampleToAuth_Feb19.Models.DTOs
{
    public class UserLoginRequestDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
