﻿using System.ComponentModel.DataAnnotations;

namespace Furniture_ExampleToAuth_Feb19.Models.DTOs
{
    public class UserRegistrationRequestDTO
    {

        [Required]
         public string Name { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
