﻿namespace Furniture_ExampleToAuth_Feb19.Configuratoins
{
    public class JwtConfig
    {
        public string Secret { get; set; }
        public TimeSpan ExpiryTimeFrame { get; set; }
    }
}
