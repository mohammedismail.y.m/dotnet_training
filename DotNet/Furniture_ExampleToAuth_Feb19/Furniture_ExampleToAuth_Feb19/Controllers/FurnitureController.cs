﻿using Furniture_ExampleToAuth_Feb19.Interface;
using Furniture_ExampleToAuth_Feb19.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Furniture_ExampleToAuth_Feb19.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class FurnitureController : ControllerBase
    {

        private readonly IFurniture _furniture;

        public FurnitureController(IFurniture furniture)
        {
            _furniture = furniture;
        }

        //Get furniture details
        [HttpGet]
        public List<Furniture> GetFurniture()
        {
            return _furniture.GetAllFurniture();
        }



        //Get employee name by id
        //[HttpGet("{id}")]
        //public string GetEmployeeName(int id)
        //{
        //    return repo.getEmployeeNameById(id);
        //}

        //Add Furniture 
        [HttpPost]
        public List<Furniture> NewFur(Furniture fur)
        {
            return _furniture.createNewFurniture(fur);
        }


        //delete furniture
        [HttpDelete("{Id}")]
        public string DeleteEmp(int Id)
        {
            var msg = _furniture.deleteFurniture(Id);
            return msg;

        }


        //update furniture
        [HttpPut]
        public List<Furniture> updateFur(Furniture fur)
        {
            return _furniture.updateFurniture(fur);

        }

    }
}
