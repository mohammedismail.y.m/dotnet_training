﻿using Furniture_ExampleToAuth_Feb19.Models;

namespace Furniture_ExampleToAuth_Feb19.Interface
{
    public interface IFurniture
    {
        List<Furniture> createNewFurniture(Furniture furniture);
        List<Furniture> GetAllFurniture();

        string deleteFurniture(int id);

        List<Furniture> updateFurniture(Furniture furniture);
    }
}
