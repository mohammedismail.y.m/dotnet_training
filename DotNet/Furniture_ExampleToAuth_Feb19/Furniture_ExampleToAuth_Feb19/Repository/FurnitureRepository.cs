﻿using Furniture_ExampleToAuth_Feb19.Interface;
using Furniture_ExampleToAuth_Feb19.Models;

namespace Furniture_ExampleToAuth_Feb19.Repository
{
    public class FurnitureRepository:IFurniture
    {
        private readonly FurnituresContext _context;

        public FurnitureRepository(FurnituresContext context)
        {
            _context = context;
        }

        public List<Furniture> createNewFurniture(Furniture furniture)
        {
            _context.Furnitures.Add(furniture);
            _context.SaveChanges();
            return _context.Furnitures.ToList();
        }

        public string deleteFurniture(int id)
        {
            var fur = _context.Furnitures.Find(id);
            _context.Furnitures.Remove(fur);
            _context.SaveChanges();


            return "Deleted successfully";
        }

        public List<Furniture> GetAllFurniture()
        {
            return _context.Furnitures.ToList();
        }

        public List<Furniture> updateFurniture(Furniture furniture)
        {
            var findFur = _context.Furnitures.Find(furniture.Id);
            if(findFur != null)
            {
                findFur.FurnitureName = furniture.FurnitureName;
                findFur.Cost = furniture.Cost;
                _context.SaveChanges();
                return _context.Furnitures.ToList();


            }

            return _context.Furnitures.ToList();
           


            

           
        }
    }
}
