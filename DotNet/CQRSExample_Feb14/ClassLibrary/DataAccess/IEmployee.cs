﻿using ClassLibrary.Commands;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DataAccess
{
    public interface IEmployee
    {

        List<Temployee> createNewEmployee(Temployee employee);
        List<Temployee> GetAllEmployees();

        string deleteEmployee(int id);

        List<Temployee> updateEmployee(Temployee employee);
        
    }
}
