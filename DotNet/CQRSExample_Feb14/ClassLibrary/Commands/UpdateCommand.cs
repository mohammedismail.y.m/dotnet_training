﻿using ClassLibrary.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Commands
{
    public record UpdateCommand(Temployee employee):IRequest<List<Temployee>>
    {
       
    }
}
