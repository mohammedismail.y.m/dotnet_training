﻿using ClassLibrary.DataAccess;
using ClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repository
{
    internal class EmployeeRepository: IEmployee
    {
        private readonly EmployeeContext _context;
        public EmployeeRepository(EmployeeContext context)
        {
            _context = context;

        }

        public List<Temployee> createNewEmployee(Temployee employee)
        {
            _context.Temployees.Add(employee);
            _context.SaveChanges();
            return _context.Temployees.ToList();
        }

        public string deleteEmployee(int id)
        {
            var emp = _context.Temployees.Find(id);
            if(emp != null)
            {
                _context.Temployees.Remove(emp);
                _context.SaveChanges();
                return "deleted succefull";
            }
            else
            {
                return "Not found";
            }
            


            
        }

        public List<Temployee> GetAllEmployees()
        {
            return _context.Temployees.ToList();
        }

        public List<Temployee> updateEmployee(Temployee employee)
        {
            var findEmp = _context.Temployees.Find(employee.EmployeeId);
            findEmp.EmpName = employee.EmpName;
            findEmp.Salary = employee.Salary;
            findEmp.Address = employee.Address;
            findEmp.Age = employee.Age;


            _context.SaveChanges();

            return _context.Temployees.ToList();
        }

    }
}
