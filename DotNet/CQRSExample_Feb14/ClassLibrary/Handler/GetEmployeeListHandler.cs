﻿using ClassLibrary.DataAccess;
using ClassLibrary.Models;
using ClassLibrary.Queries;

using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Handler
{
    public class GetEmployeeListHandler : IRequestHandler<GetEmployeeList, List<Temployee>>
    {

        private readonly IEmployee _employee;

        public GetEmployeeListHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<Temployee>> Handle(GetEmployeeList request, CancellationToken cancellationToken)
        {
            //return Task.FromResult(new List<Temployee>());
            return Task.FromResult(_employee.GetAllEmployees());

        }
    }
}
