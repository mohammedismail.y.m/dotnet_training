﻿using ClassLibrary.Commands;
using ClassLibrary.DataAccess;
using ClassLibrary.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Handler
{
    public class UpdateHandler:IRequestHandler<UpdateCommand, List<Temployee>>
    {
        private readonly IEmployee _employee;

        public UpdateHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<Temployee>> Handle(UpdateCommand request, CancellationToken cancellationToken)
        {
           
            return Task.FromResult(_employee.updateEmployee(request.employee));
        }
    }
}
