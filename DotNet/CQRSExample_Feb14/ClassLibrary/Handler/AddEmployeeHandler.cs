﻿using ClassLibrary.Commands;
using ClassLibrary.Queries;
using ClassLibrary.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary.DataAccess;

namespace ClassLibrary.Handler
{
    internal class AddEmployeeHandler : IRequestHandler<AddEmployee, List<Temployee>>
    {
        private readonly IEmployee _employee;

        public AddEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }
        public Task<List<Temployee>> Handle(AddEmployee request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            
            return Task.FromResult(_employee.createNewEmployee(request.employee));
        }
    }

}
