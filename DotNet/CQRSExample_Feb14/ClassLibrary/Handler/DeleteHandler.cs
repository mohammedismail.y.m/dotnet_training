﻿using ClassLibrary.Commands;
using ClassLibrary.DataAccess;
using ClassLibrary.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Handler
{
    public class DeleteHandler : IRequestHandler<DeleteCommand, string>
    {
        private readonly IEmployee _employee;

        public DeleteHandler(IEmployee employee)
        {
            _employee = employee;
        }

       

        Task<string> IRequestHandler<DeleteCommand, string>.Handle(DeleteCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.deleteEmployee(request.id));
        }
    }
}
