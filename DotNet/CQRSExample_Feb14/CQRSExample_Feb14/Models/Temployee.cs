﻿using System;
using System.Collections.Generic;

namespace CQRSExample_Feb14.Models
{
    public partial class Temployee
    {
        public int EmployeeId { get; set; }
        public string? EmpName { get; set; }
        public string? Address { get; set; }
        public int? Salary { get; set; }
        public int? Age { get; set; }
    }
}
