﻿using ClassLibrary.Commands;
using Employee_CQRS_Feb15.Models;
using Employee_CQRS_Feb15.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Employee_CQRS_Feb15.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetEmployeeList()
        {
            var employeeList =  _mediator.Send(new GetEmployeeList());
            return Ok(employeeList);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromBody] Temployee employee)
        {
            await _mediator.Send(new AddEmployee(employee));
            return StatusCode(201);

        }

        [HttpPut]
        public async Task<IActionResult> UpdateEmployee([FromBody] Temployee employee)
        {
            await _mediator.Send(new UpdateCommand(employee));
            return StatusCode(201);

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee([FromBody] int id)
        {
            await _mediator.Send(new DeleteCommand(id));
            return StatusCode(200);
        }
    }
}
