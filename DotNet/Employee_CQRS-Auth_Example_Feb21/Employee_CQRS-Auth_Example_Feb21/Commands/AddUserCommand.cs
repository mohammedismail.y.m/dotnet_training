﻿using Employee_CQRS_Auth_Example_Feb21.Models;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Commands
{
    public record AddUserCommand(UserDetail newUser) : IRequest<List<UserDetail>>
    {
        
    }
}
