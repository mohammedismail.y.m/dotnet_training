﻿using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Commands
{
    public record DeleteEmployeeCommand(int Id): IRequest<string>
    {
    }
}
