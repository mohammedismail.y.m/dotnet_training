﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class AddEmployeeHandler : IRequestHandler<AddEmployeeCommand, List<EmployeeDetail>>
    {
        private readonly IEmployee _employee;

        public AddEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<EmployeeDetail>> Handle(AddEmployeeCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.AddNewEmployee(request.employee));
        }
    }
}
