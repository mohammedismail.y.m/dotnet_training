﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class UpdateEmployeeHandler : IRequestHandler<UpdateEmployeeCommand, string>
    {
        private readonly IEmployee _employee;

        public UpdateEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<string> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.UpdateEmployee(request.updateEmployee));
        }
    }
}
