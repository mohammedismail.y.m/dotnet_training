﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class DeleteEmployeeHandler : IRequestHandler<DeleteEmployeeCommand, string>
    {
        private readonly IEmployee _employee;

        public DeleteEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<string> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.DeleteEmployee(request.Id));
        }
    }
}
