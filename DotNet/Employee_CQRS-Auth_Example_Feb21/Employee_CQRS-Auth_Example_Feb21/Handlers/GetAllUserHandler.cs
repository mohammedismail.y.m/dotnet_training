﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class GetAllUserHandler : IRequestHandler<GetAllUsersQuery, List<UserDetail>>
    {
        private readonly IUser _user;

        public GetAllUserHandler(IUser user)
        {
            _user = user;
        }

        public Task<List<UserDetail>> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_user.GetAllUserDetails());
        }
    }
}
