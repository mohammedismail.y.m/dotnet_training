﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class GetAllEmployeesHandler : IRequestHandler<GetAllEmployeesQuery, List<EmployeeDetail>>
    {
        private readonly IEmployee _employee;

        public GetAllEmployeesHandler(IEmployee employee)
        {
            _employee = employee;
        }

        public Task<List<EmployeeDetail>> Handle(GetAllEmployeesQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_employee.GetEmployeeDetails());
        }
    }
}
