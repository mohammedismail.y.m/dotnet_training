﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class CheckEmailAndPasswordHandler : IRequestHandler<CheckEmailAndPasswordQuery, AuthResult>
    {
        private readonly IAuth _auth;

        public CheckEmailAndPasswordHandler(IAuth auth)
        {
            _auth = auth;
        }

       
           

        Task<AuthResult> IRequestHandler<CheckEmailAndPasswordQuery, AuthResult>.Handle(CheckEmailAndPasswordQuery request, CancellationToken cancellationToken)
        {
               return Task.FromResult(_auth.login(request.loginUser));
            
        }
    }
}
