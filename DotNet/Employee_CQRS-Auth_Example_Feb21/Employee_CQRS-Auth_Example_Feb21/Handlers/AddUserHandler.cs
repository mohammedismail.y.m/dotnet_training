﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class AddUserHandler : IRequestHandler<AddUserCommand, List<UserDetail>>
    {
        private readonly IUser _user;

        public AddUserHandler(IUser user)
        {
            _user = user;
        }

        public Task<List<UserDetail>> Handle(AddUserCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_user.AddNewUser(request.newUser));
        }
    }
}
