﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class FindUserByEmailHandler : IRequestHandler<FindUserByEmail, string>
    {
        private readonly IAuth _auth;

        public FindUserByEmailHandler(IAuth auth)
        {
            _auth = auth;
        }

       

        Task<string> IRequestHandler<FindUserByEmail, string>.Handle(FindUserByEmail request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_auth.register(request.newUser));
               
        }
    }
}
