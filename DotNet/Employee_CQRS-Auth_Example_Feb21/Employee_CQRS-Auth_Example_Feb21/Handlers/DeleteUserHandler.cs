﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class DeleteUserHandler : IRequestHandler<DeleteUserCommand, string>
    {
        private readonly IUser _user;

        public DeleteUserHandler(IUser user)
        {
            _user = user;
        }

        public Task<string> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_user.DeleteUser(request.id));
        }
    }
}
