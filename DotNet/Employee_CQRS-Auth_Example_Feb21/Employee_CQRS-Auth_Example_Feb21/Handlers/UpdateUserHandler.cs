﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Handlers
{
    public class UpdateUserHandler : IRequestHandler<UpdateUserCommand, string>
    {
        private readonly IUser _user;
        public UpdateUserHandler(IUser user)
        {
            _user = user;
        }

        public Task<string> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_user.UpdateUser(request.updateUser));
        }
    }
}
