﻿using Employee_CQRS_Auth_Example_Feb21.Models;

namespace Employee_CQRS_Auth_Example_Feb21.DataAccess
{
    public interface IEmployee
    {
        List<EmployeeDetail> GetEmployeeDetails();

        List<EmployeeDetail> AddNewEmployee(EmployeeDetail employee);

        string UpdateEmployee(EmployeeDetail employee);

        string DeleteEmployee(int Id);
    }
}
