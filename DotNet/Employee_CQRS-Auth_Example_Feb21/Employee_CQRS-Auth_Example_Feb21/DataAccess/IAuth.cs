﻿using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Models.DTO;

namespace Employee_CQRS_Auth_Example_Feb21.DataAccess
{
    public interface IAuth
    {
        AuthResult login(UserLoginRequestDto user);

        string register(UserRegisterDto newUser);

    }
}
