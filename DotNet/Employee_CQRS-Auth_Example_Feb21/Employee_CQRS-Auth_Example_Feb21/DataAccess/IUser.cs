﻿using Employee_CQRS_Auth_Example_Feb21.Models;

namespace Employee_CQRS_Auth_Example_Feb21.DataAccess
{
    public interface IUser
    {
        List<UserDetail> GetAllUserDetails();

        List<UserDetail> AddNewUser(UserDetail newUser);

        string UpdateUser(UserDetail updateUser);

        string DeleteUser(int Id);

        bool FindUserByEmail(UserDetail user);

        List<UserDetail> CheckCredentials(UserDetail user);
    }
}
