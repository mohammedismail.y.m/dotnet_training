﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Employee_CQRS_Auth_Example_Feb21.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UserController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet, Authorize(Roles = "admin")]
        public async Task<IActionResult> GetAllUser()
        {
            var userList = _mediator.Send(new GetAllUsersQuery());
            return Ok(userList);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewUser([FromBody] UserDetail newUser)
        {
            await _mediator.Send(new AddUserCommand(newUser));
            return StatusCode(201);
        }

        [HttpPut]
        public async Task<IActionResult> UpdateUser([FromBody] UserDetail updateUser)
        {
            await _mediator.Send(new UpdateUserCommand(updateUser));
            return StatusCode(201);
        }

        [HttpDelete]
        [Route("{DeleteUSer}")]
        public async Task<IActionResult> DeleteUser([FromBody] int id)
        {

            await _mediator.Send(new DeleteUserCommand(id));
            return StatusCode(200);
        }


    }
}
