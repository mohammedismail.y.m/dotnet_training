﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Employee_CQRS_Auth_Example_Feb21.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [HttpGet]
        public async Task<IActionResult> GetAllEmployee()
        {
            var employeeList = _mediator.Send(new GetAllEmployeesQuery());
            return Ok(employeeList);
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee([FromBody] EmployeeDetail newEmployee)
        {
            await _mediator.Send(new AddEmployeeCommand(newEmployee));
            return StatusCode(201);
        }

        [HttpPut]
        public async Task<IActionResult> updateEmployee([FromBody] EmployeeDetail updateEmployee)
        {
            await _mediator.Send(new UpdateEmployeeCommand(updateEmployee));
            return StatusCode(201);
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee([FromBody] int Id)
        {
            await _mediator.Send(new DeleteEmployeeCommand(Id));
            return StatusCode(200);
        }
    }
}
