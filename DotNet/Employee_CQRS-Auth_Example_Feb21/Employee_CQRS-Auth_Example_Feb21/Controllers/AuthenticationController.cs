﻿using Employee_CQRS_Auth_Example_Feb21.Commands;
using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Models.DTO;
using Employee_CQRS_Auth_Example_Feb21.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Employee_CQRS_Auth_Example_Feb21.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        //This class provides the login and register functionalties

        

        private readonly IMediator _mediator;

        

        public AuthenticationController( IMediator mediator)
        {
            
            _mediator = mediator;
        }

        [HttpPost]
        [Route("Register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] UserRegisterDto requestUser)
        {
            

            var save_user = await _mediator.Send(new FindUserByEmail(requestUser));

            return Ok(save_user);

        }
        [AllowAnonymous]
        [Route("login")]
        [HttpPost]
        
        public async Task<IActionResult> Login([FromBody] UserLoginRequestDto loginRequest)
        {
            var result = await _mediator.Send(new CheckEmailAndPasswordQuery(loginRequest));

            return Ok(result);
        }

        

       
    }
}
