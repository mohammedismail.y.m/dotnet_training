﻿using Employee_CQRS_Auth_Example_Feb21.Models;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Queries
{
    public record GetAllUsersQuery:IRequest<List<UserDetail>>
    {
    }
}
