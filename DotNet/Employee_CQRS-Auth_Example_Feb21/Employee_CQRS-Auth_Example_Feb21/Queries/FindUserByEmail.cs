﻿using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Models.DTO;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Queries
{
    //this to register new user, first will verify and if user not present add to database.
    public record FindUserByEmail(UserRegisterDto newUser):IRequest<string>
    {
    }
}
