﻿using Employee_CQRS_Auth_Example_Feb21.Models;
using Employee_CQRS_Auth_Example_Feb21.Models.DTO;
using MediatR;

namespace Employee_CQRS_Auth_Example_Feb21.Queries
{
    //to perform check on login credentials
    public record CheckEmailAndPasswordQuery(UserLoginRequestDto loginUser):IRequest<AuthResult>
    {
    }
}
