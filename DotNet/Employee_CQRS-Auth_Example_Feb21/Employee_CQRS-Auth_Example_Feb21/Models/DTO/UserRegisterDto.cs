﻿using System.ComponentModel.DataAnnotations;

namespace Employee_CQRS_Auth_Example_Feb21.Models.DTO
{
    public class UserRegisterDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
