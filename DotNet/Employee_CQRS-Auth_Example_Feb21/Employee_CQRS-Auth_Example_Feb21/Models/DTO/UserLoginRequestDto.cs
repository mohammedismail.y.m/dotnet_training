﻿using System.ComponentModel.DataAnnotations;

namespace Employee_CQRS_Auth_Example_Feb21.Models.DTO
{
    public class UserLoginRequestDto
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
