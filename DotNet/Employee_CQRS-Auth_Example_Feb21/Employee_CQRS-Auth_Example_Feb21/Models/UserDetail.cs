﻿using System;
using System.Collections.Generic;

namespace Employee_CQRS_Auth_Example_Feb21.Models;

public partial class UserDetail
{
    public int Id { get; set; }

    public string? UserName { get; set; }

    public string? Email { get; set; }

    public string? Password { get; set; }

    public string? Role { get; set; }
}
