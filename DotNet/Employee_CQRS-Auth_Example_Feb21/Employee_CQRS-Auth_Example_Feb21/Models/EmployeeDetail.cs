﻿using System;
using System.Collections.Generic;

namespace Employee_CQRS_Auth_Example_Feb21.Models;

public partial class EmployeeDetail
{
    public int Id { get; set; }

    public string? EmployeeName { get; set; }

    public int? EmployeeSalary { get; set; }

    public string? EmployeeDesignation { get; set; }
}
