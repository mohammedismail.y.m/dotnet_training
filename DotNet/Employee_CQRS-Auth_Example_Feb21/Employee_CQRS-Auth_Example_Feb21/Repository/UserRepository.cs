﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;

namespace Employee_CQRS_Auth_Example_Feb21.Repository
{
    public class UserRepository:IUser
    {
        private readonly EmployeeAuth2Context _context;

        public UserRepository(EmployeeAuth2Context context)
        {
            _context = context;
        }

        public List<UserDetail> AddNewUser(UserDetail newUser)
        {
            _context.UserDetails.Add(newUser);
            _context.SaveChanges();
            
            return _context.UserDetails.ToList();
        }

        public List<UserDetail> CheckCredentials(UserDetail user)
        {
            var currentUser = _context.UserDetails.Where(o => o.Email == user.Email
            && o.Password == user.Password).ToList();

            if(currentUser != null) {

                return currentUser;
            
            }
            return null;
        }

        public string DeleteUser(int Id)
        {
            var checkEmp = _context.UserDetails.Find(Id);
            if (checkEmp != null)
            {

                _context.UserDetails.Remove(checkEmp);
                _context.SaveChanges();
                return "deleted successfully";


            }

            return "Not Found";
        }

        public bool FindUserByEmail(UserDetail user)
        {
            var check = _context.UserDetails.Where(p => p.Email == user.Email).ToList();
            if (check != null)
            {
                return true;
            }
            return false;
        }

        public List<UserDetail> GetAllUserDetails()
        {
            return _context.UserDetails.ToList();
        }

        public string UpdateUser(UserDetail updateUser)
        {
            var checkEmp = _context.UserDetails.Find(updateUser.Id);
            if (checkEmp != null) { 

                checkEmp.UserName = updateUser.UserName;
                checkEmp.Password = updateUser.Password;
                checkEmp.Email = updateUser.Email;
                _context.SaveChanges();
                return "Updated successfully";
            
            
            }

            return "Not Found";
        }
    }
}
