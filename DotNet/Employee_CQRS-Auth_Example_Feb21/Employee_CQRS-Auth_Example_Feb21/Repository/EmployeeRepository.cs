﻿using Employee_CQRS_Auth_Example_Feb21.DataAccess;
using Employee_CQRS_Auth_Example_Feb21.Models;

namespace Employee_CQRS_Auth_Example_Feb21.Repository
{

    //This class provides crud operations



    //custom exceptions
    public class NotFoundException : Exception
    {
        public NotFoundException(string message):base(message) { }

    }


    //Using DbContext we are performing operations on data
    public class EmployeeRepository : IEmployee
    {

        private readonly EmployeeAuth2Context _context;

        public EmployeeRepository(EmployeeAuth2Context context)
        {
            _context = context;
            
        }

        public List<EmployeeDetail> AddNewEmployee(EmployeeDetail employee)
        {
            _context.EmployeeDetails.Add(employee);
            _context.SaveChanges();
            Console.WriteLine("Employee added successfully");
            return new List<EmployeeDetail>();
        }

        public string DeleteEmployee(int Id)
        {
            var checkEmp = _context.EmployeeDetails.Find(Id);
            if (checkEmp != null)
            {
                _context.EmployeeDetails.Remove(checkEmp);
                _context.SaveChanges();
                return "Deleted successfully";
            }

           throw new NotFoundException( "Not found");

        }

        public List<EmployeeDetail> GetEmployeeDetails()
        {
            return _context.EmployeeDetails.ToList();
        }

        public string UpdateEmployee(EmployeeDetail employee)
        {
            var checkEmp = _context.EmployeeDetails.Find(employee.Id);
            if (checkEmp != null)
            {
                checkEmp.EmployeeName = employee.EmployeeName;
                checkEmp.EmployeeSalary = employee.EmployeeSalary;
                checkEmp.EmployeeDesignation = employee.EmployeeDesignation;
                _context.SaveChanges();
                return "updated successfully";
            }

            return "Not found";
        }
    }
}
