﻿using EmployeeWEBApi_Feb13.Interfaces;
using EmployeeWEBApi_Feb13.Models;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeWEBApi_Feb13.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : Controller
    {
        private readonly IEmployee empRepo;

        public  EmployeeController(IEmployee empRepo)
        {
            this.empRepo = empRepo;
        }

        [HttpPost]
        public List<Temployee> AddNewEmployee(Temployee employee)
        {
            return empRepo.AddNewEmployee(employee);
        }
        
    }
}
