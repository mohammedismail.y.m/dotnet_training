﻿using EmployeeWEBApi_Feb13.Interfaces;
using EmployeeWEBApi_Feb13.Models;

namespace EmployeeWEBApi_Feb13.Repository
{
    public class EmployeeRepository : IEmployee
    {
        private readonly EmployeeContext _context;

        public EmployeeRepository(EmployeeContext context)
        {
            _context = context;
        }
        public List<Temployee> AddNewEmployee(Temployee employee)
        {
            _context.Temployees.Add(employee);
            _context.SaveChanges();
            return _context.Temployees.ToList();
        }
    }
}
