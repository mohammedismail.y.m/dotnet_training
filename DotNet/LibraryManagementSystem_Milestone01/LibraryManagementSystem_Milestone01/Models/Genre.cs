﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem_Milestone01.Models
{
    public partial class Genre
    {
        //This class provides the data fields needed for Genre
        public Genre()
        {
            Books = new HashSet<Book>();
        }

        public int GenreId { get; set; }
        public string? Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
