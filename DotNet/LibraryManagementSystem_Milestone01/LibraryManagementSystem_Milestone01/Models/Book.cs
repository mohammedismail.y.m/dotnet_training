﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem_Milestone01.Models
{
    public partial class Book
    {
        //This class provides the data fields needed for Books collection
        public Book()
        {
            BorrowLedgers = new HashSet<BorrowLedger>();
        }

        public int BookId { get; set; }
        public string? Name { get; set; }
        public string? PageCount { get; set; }
        public int? AuthorId { get; set; }
        public int? GenreId { get; set; }

        public virtual Author? Author { get; set; }
        public virtual Genre? Genre { get; set; }
        public virtual ICollection<BorrowLedger> BorrowLedgers { get; set; }
    }
}
