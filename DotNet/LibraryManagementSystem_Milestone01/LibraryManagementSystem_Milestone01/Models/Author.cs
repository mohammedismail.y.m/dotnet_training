﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem_Milestone01.Models
{
    public partial class Author
    {
        //This class provides the data fields needed for collection of Author details
        public Author()
        {
            Books = new HashSet<Book>();
        }

        public int AuthorId { get; set; }
        public string? Name { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
