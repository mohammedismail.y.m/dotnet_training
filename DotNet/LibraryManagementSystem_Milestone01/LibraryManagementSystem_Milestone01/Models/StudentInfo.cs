﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem_Milestone01.Models
{
    public partial class StudentInfo
    {
        //This class provides the data fields needed to collect Student Details
        public StudentInfo()
        {
            BorrowLedgers = new HashSet<BorrowLedger>();
        }

        public int StudentId { get; set; }
        public string? Name { get; set; }
        public string? DateOfBirth { get; set; }
        public string? Gender { get; set; }
        public int? Class { get; set; }

        public virtual ICollection<BorrowLedger> BorrowLedgers { get; set; }
    }
}
