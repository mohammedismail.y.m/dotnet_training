﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementSystem_Milestone01.Models;

namespace LibraryManagementSystem_Milestone01.Controllers
{
    public class StudentInfoesController : Controller
    {
        //Controller for Student

        private readonly LibraryContext _context;

        public StudentInfoesController(LibraryContext context)
        {
            _context = context;
        }

        // GET: StudentInfoes
        public async Task<IActionResult> Index()
        {
              return View(await _context.StudentInfos.ToListAsync());
        }

        // GET: StudentInfoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.StudentInfos == null)
            {
                return NotFound();
            }

            var studentInfo = await _context.StudentInfos
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (studentInfo == null)
            {
                return NotFound();
            }

            return View(studentInfo);
        }

        // GET: StudentInfoes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StudentInfoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentId,Name,DateOfBirth,Gender,Class")] StudentInfo studentInfo)
        {
            if (ModelState.IsValid)
            {
                _context.Add(studentInfo);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(studentInfo);
        }

        // GET: StudentInfoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.StudentInfos == null)
            {
                return NotFound();
            }

            var studentInfo = await _context.StudentInfos.FindAsync(id);
            if (studentInfo == null)
            {
                return NotFound();
            }
            return View(studentInfo);
        }

        // POST: StudentInfoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentId,Name,DateOfBirth,Gender,Class")] StudentInfo studentInfo)
        {
            if (id != studentInfo.StudentId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studentInfo);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentInfoExists(studentInfo.StudentId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(studentInfo);
        }

        // GET: StudentInfoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.StudentInfos == null)
            {
                return NotFound();
            }

            var studentInfo = await _context.StudentInfos
                .FirstOrDefaultAsync(m => m.StudentId == id);
            if (studentInfo == null)
            {
                return NotFound();
            }

            return View(studentInfo);
        }

        // POST: StudentInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.StudentInfos == null)
            {
                return Problem("Entity set 'LibraryContext.StudentInfos'  is null.");
            }
            var studentInfo = await _context.StudentInfos.FindAsync(id);
            if (studentInfo != null)
            {
                _context.StudentInfos.Remove(studentInfo);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentInfoExists(int id)
        {
          return _context.StudentInfos.Any(e => e.StudentId == id);
        }
    }
}
