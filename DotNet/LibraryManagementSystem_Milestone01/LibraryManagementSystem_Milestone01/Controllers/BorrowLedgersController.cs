﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LibraryManagementSystem_Milestone01.Models;

namespace LibraryManagementSystem_Milestone01.Controllers
{
    public class BorrowLedgersController : Controller
    {
        //Controller for BorrowLedger or LibraryLedger

        private readonly LibraryContext _context;

        public BorrowLedgersController(LibraryContext context)
        {
            _context = context;
        }

        // GET: BorrowLedgers
        public async Task<IActionResult> Index()
        {
            var libraryContext = _context.BorrowLedgers.Include(b => b.Book).Include(b => b.Student);
            return View(await libraryContext.ToListAsync());
        }

        // GET: BorrowLedgers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.BorrowLedgers == null)
            {
                return NotFound();
            }

            var borrowLedger = await _context.BorrowLedgers
                .Include(b => b.Book)
                .Include(b => b.Student)
                .FirstOrDefaultAsync(m => m.BorrowId == id);
            if (borrowLedger == null)
            {
                return NotFound();
            }

            return View(borrowLedger);
        }

        // GET: BorrowLedgers/Create
        public IActionResult Create()
        {
            /*ViewData["BookId"] = new SelectList(_context.Books, "BookId", "BookId");
            ViewData["StudentId"] = new SelectList(_context.StudentInfos, "StudentId", "StudentId");*/
            var studentsList = (from student in _context.StudentInfos
                               select new SelectListItem()
                               {
                                   Text = student.Name,
                                   Value = student.StudentId.ToString(),
                               }).ToList();
            var booksList = (from book in _context.Books
                              select new SelectListItem()
                              {
                                  Text = book.Name,
                                  Value = book.BookId.ToString(),
                              }).ToList();


            ViewBag.Listofbooks = booksList;
            ViewBag.Listofstudents = studentsList;

            return View();
        }

        // POST: BorrowLedgers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("BorrowId,StudentId,BookId,TakenDate,ReturnDate")] BorrowLedger borrowLedger)
        {
            if (ModelState.IsValid)
            {
                _context.Add(borrowLedger);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Books, "BookId", "BookId", borrowLedger.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentInfos, "StudentId", "StudentId", borrowLedger.StudentId);
            return View(borrowLedger);
        }

        // GET: BorrowLedgers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.BorrowLedgers == null)
            {
                return NotFound();
            }

            var borrowLedger = await _context.BorrowLedgers.FindAsync(id);
            if (borrowLedger == null)
            {
                return NotFound();
            }
            /*ViewData["BookId"] = new SelectList(_context.Books, "BookId", "BookId", borrowLedger.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentInfos, "StudentId", "StudentId", borrowLedger.StudentId);
       */     var studentsList = (from student in _context.StudentInfos
                                select new SelectListItem()
                                {
                                    Text = student.Name,
                                    Value = student.StudentId.ToString(),
                                }).ToList();
            var booksList = (from book in _context.Books
                             select new SelectListItem()
                             {
                                 Text = book.Name,
                                 Value = book.BookId.ToString(),
                             }).ToList();


            ViewBag.Listofbooks = booksList;
            ViewBag.Listofstudents = studentsList;

            return View(borrowLedger);
        }

        // POST: BorrowLedgers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BorrowId,StudentId,BookId,TakenDate,ReturnDate")] BorrowLedger borrowLedger)
        {
            if (id != borrowLedger.BorrowId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(borrowLedger);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BorrowLedgerExists(borrowLedger.BorrowId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BookId"] = new SelectList(_context.Books, "BookId", "BookId", borrowLedger.BookId);
            ViewData["StudentId"] = new SelectList(_context.StudentInfos, "StudentId", "StudentId", borrowLedger.StudentId);
            return View(borrowLedger);
        }

        // GET: BorrowLedgers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.BorrowLedgers == null)
            {
                return NotFound();
            }

            var borrowLedger = await _context.BorrowLedgers
                .Include(b => b.Book)
                .Include(b => b.Student)
                .FirstOrDefaultAsync(m => m.BorrowId == id);
            if (borrowLedger == null)
            {
                return NotFound();
            }

            return View(borrowLedger);
        }

        // POST: BorrowLedgers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.BorrowLedgers == null)
            {
                return Problem("Entity set 'LibraryContext.BorrowLedgers'  is null.");
            }
            var borrowLedger = await _context.BorrowLedgers.FindAsync(id);
            if (borrowLedger != null)
            {
                _context.BorrowLedgers.Remove(borrowLedger);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BorrowLedgerExists(int id)
        {
          return _context.BorrowLedgers.Any(e => e.BorrowId == id);
        }
    }
}
