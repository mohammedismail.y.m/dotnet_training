﻿using System;
using System.Collections.Generic;

namespace ExampleToPracticeMVC.Models;

public partial class StudentInfo
{
    public int StudentId { get; set; }

    public string? Name { get; set; }

    public string? DateOfBirth { get; set; }

    public string? Gender { get; set; }

    public int? Class { get; set; }

    public virtual ICollection<BorrowLedger> BorrowLedgers { get; } = new List<BorrowLedger>();
}
