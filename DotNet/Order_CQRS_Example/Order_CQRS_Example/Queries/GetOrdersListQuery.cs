﻿using MediatR;
using Order_CQRS_Example.Models;

namespace Order_CQRS_Example.Queries
{
    public record GetOrdersListQuery:IRequest<List<Order>>
    {
    }
}
