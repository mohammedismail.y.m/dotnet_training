﻿using System;
using System.Collections.Generic;

namespace Order_CQRS_Example.Models;

public partial class Order
{
    public int OrderId { get; set; }

    public string? OrderName { get; set; }
}
