﻿using Order_CQRS_Example.Data_Access;
using Order_CQRS_Example.Models;

namespace Order_CQRS_Example.Repository
{
    public class OrderRepository : IOrders
    {
        private readonly OrdersContext _context;

        public OrderRepository(OrdersContext context)
        {
            _context = context;
        }
        public List<Order> CreateNew(Order order)
        {
            _context.Orders.Add(order);
            _context.SaveChanges();
            return _context.Orders.ToList();
        }

        public string DeleteOrder(int id)
        {
            var getOrderId = _context.Orders.Find(id);
            if (getOrderId != null)
            {
                _context.Orders.Remove(getOrderId);
                _context.SaveChanges();
                return "deleted successfully";
            }
            else
            {
                return "Not found";
            }
        }

        public List<Order> GetOrderList()
        {
            return _context.Orders.ToList();
        }

        public List<Order> updateOrder(Order order)
        {
            var getOrdersId = _context.Orders.Find(order.OrderId);
            if (getOrdersId != null)
            {
                getOrdersId.OrderName= getOrdersId.OrderName;

                _context.SaveChanges();
                return _context.Orders.ToList();

            }
            else
            {
                return _context.Orders.ToList();
            }
        }
    }
}
