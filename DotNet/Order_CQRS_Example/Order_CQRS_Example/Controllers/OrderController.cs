﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Order_CQRS_Example.Commands;
using Order_CQRS_Example.Models;
using Order_CQRS_Example.Queries;

namespace Order_CQRS_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;

        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetOrderList()
        {
            var orderList = _mediator.Send(new GetOrdersListQuery());
            return Ok(orderList);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePatient([FromBody] Order order)
        {
            await _mediator.Send(new AddOrderCommand(order));
            return StatusCode(201);

        }

       /* [HttpPut]
        public async Task<IActionResult> UpdateEmployee([FromBody] Patient patient)
        {
            await _mediator.Send(new UpdatePatientCommand(patient));
            return StatusCode(201);

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee([FromBody] int id)
        {
            await _mediator.Send(new DeletePatientCommand(id));
            return StatusCode(200);
        }*/
    }
}
