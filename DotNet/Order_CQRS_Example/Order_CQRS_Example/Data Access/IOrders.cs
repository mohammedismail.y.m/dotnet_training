﻿using Order_CQRS_Example.Models;

namespace Order_CQRS_Example.Data_Access
{
    public interface IOrders
    {
        List<Order> GetOrderList();

        //Insert a new record
        public List<Order> CreateNew(Order order);

        //update a record
        public List<Order> updateOrder(Order order);

        //delete a record
        public string DeleteOrder(int id);
    }
}
