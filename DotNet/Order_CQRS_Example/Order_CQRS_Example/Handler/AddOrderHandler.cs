﻿using MediatR;
using Order_CQRS_Example.Commands;
using Order_CQRS_Example.Data_Access;
using Order_CQRS_Example.Models;

namespace Order_CQRS_Example.Handler
{
    public class AddOrderHandler : IRequestHandler<AddOrderCommand, List<Order>>
    {
        private readonly IOrders _order;

        public AddOrderHandler(IOrders order)
        {
              _order= order;
        }
        public async Task<List<Order>> Handle(AddOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_order.CreateNew(request.order));
        }
    }
}
