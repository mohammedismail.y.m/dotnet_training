﻿using MediatR;
using Order_CQRS_Example.Data_Access;
using Order_CQRS_Example.Models;
using Order_CQRS_Example.Queries;

namespace Order_CQRS_Example.Handler
{
    public class GetOrderListHandler : IRequestHandler<GetOrdersListQuery, List<Order>>
    {
        private readonly IOrders _order;

        public GetOrderListHandler(IOrders order) {
            _order = order;       
        }
        public async Task<List<Order>> Handle(GetOrdersListQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_order.GetOrderList().ToList());
        }
    }
}
