﻿using MediatR;
using Order_CQRS_Example.Models;

namespace Order_CQRS_Example.Commands
{
    public record AddOrderCommand(Order order): IRequest<List<Order>>
    {
    }
}
