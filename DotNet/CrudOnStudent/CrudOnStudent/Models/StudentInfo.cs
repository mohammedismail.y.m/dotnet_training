﻿using System;
using System.Collections.Generic;

namespace CrudOnStudent.Models
{
    public partial class StudentInfo
    {
        public int StudentId { get; set; }
        public string? StudentName { get; set; }
        public string? Address { get; set; }
        public int? CourseId { get; set; }
    }
}
