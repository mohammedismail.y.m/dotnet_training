﻿using System;
using System.Collections.Generic;

namespace CrudOnStudent.Models
{
    public partial class Course
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; } = null!;
        public int? Fees { get; set; }
    }
}
