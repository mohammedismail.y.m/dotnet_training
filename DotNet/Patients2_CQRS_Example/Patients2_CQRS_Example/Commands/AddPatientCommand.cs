﻿using MediatR;
using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.Commands
{
    public record AddPatientCommand(Patient patient):IRequest<List<Patient>>
    {
    }
}
