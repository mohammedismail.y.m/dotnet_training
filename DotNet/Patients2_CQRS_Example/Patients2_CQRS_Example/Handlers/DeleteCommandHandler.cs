﻿using MediatR;
using Patients2_CQRS_Example.Commands;
using Patients2_CQRS_Example.DataAccess;

namespace Patients2_CQRS_Example.Handlers
{
    public class DeleteCommandHandler : IRequestHandler<DeletePatientCommand, string>
    {
        private readonly IPatients _patient;

        public DeleteCommandHandler(IPatients patient)
        {
            _patient = patient;
        }

        public async Task<string> Handle(DeletePatientCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_patient.DeletePatient(request.id));
        }
    }
}
