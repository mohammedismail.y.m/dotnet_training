﻿using MediatR;
using Patients2_CQRS_Example.Commands;
using Patients2_CQRS_Example.DataAccess;
using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.Handlers
{
    public class AddPatientHandler : IRequestHandler<AddPatientCommand, List<Patient>>
    {
        private readonly IPatients _patients;

        public AddPatientHandler(IPatients patients)
        {
            _patients = patients;
        }

        public async Task<List<Patient>> Handle(AddPatientCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_patients.CreateNew(request.patient));
        }
    }
}
