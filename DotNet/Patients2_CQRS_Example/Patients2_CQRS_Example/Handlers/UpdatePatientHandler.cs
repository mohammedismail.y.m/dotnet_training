﻿using MediatR;
using Patients2_CQRS_Example.Commands;
using Patients2_CQRS_Example.DataAccess;
using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.Handlers
{
    public class UpdatePatientHandler : IRequestHandler<UpdatePatientCommand, List<Patient>>
    {
        private readonly IPatients _patient;

        public UpdatePatientHandler(IPatients patient)
        {
            _patient = patient;
        }

        public async Task<List<Patient>> Handle(UpdatePatientCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_patient.updatePatient(request.patient));
        }
    }
}
