﻿using MediatR;
using Patients2_CQRS_Example.DataAccess;
using Patients2_CQRS_Example.Models;
using Patients2_CQRS_Example.Queries;

namespace Patients2_CQRS_Example.Handlers
{
    public class GetPatientsListHandler : IRequestHandler<GetPatientsListQuery, List<Patient>>
    {
        private readonly IPatients _patient;

        public GetPatientsListHandler(IPatients patient)
        {
            _patient = patient;
        }
        public async Task<List<Patient>> Handle(GetPatientsListQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_patient.GetPatientList());
        }
    }
}
