﻿using MediatR;
using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.Queries
{
    public record GetPatientsListQuery: IRequest<List<Patient>>
    {
    }
}
