﻿using Patients2_CQRS_Example.DataAccess;
using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.Repository
{
    public class PatientsRepository : IPatients
    {
        private readonly PatientsContext _context;

        public PatientsRepository(PatientsContext context)
        {
            _context = context;
        }

        public List<Patient> CreateNew(Patient patient)
        {
            _context.Patients.Add(patient);
            _context.SaveChanges();
            return _context.Patients.ToList();
        }

       

        public string DeletePatient(int id)
        {
            var getPatientId = _context.Patients.Find(id);
            if (getPatientId != null)
            {
                _context.Patients.Remove(getPatientId);
                _context.SaveChanges();
                return "deleted successfully";
            }
            else
            {
                return "Not found";
            }
        }

        

        public List<Patient> GetPatientList()
        {
            return _context.Patients.ToList();
        }

        public List<Patient> updatePatient(Patient patient)
        {


            var getPatientId = _context.Patients.Find(patient.PatientId);
            if (getPatientId != null)
            {
                getPatientId.PatientName = patient.PatientName;

                _context.SaveChanges();
                return _context.Patients.ToList();

            }
            else
            {
                return _context.Patients.ToList();
            }
        }

       
    }
}
