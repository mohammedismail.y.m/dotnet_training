﻿using Patients2_CQRS_Example.Models;

namespace Patients2_CQRS_Example.DataAccess
{
    public interface IPatients
    {
        List<Patient> GetPatientList();

        //Insert a new record
        public List<Patient> CreateNew(Patient patient);

        //update a record
        public List<Patient> updatePatient(Patient patient);

        //delete a record
        public string DeletePatient(int id);
    }
}
