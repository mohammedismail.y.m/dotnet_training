﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Patients2_CQRS_Example.Commands;
using Patients2_CQRS_Example.Models;
using Patients2_CQRS_Example.Queries;

namespace Patients2_CQRS_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PatientController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PatientController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetPatientList()
        {
            var patientList = _mediator.Send(new GetPatientsListQuery());
            return Ok(patientList);
        }

        [HttpPost]
        public async Task<IActionResult> CreatePatient([FromBody] Patient patient)
        {
            await _mediator.Send(new AddPatientCommand(patient));
            return StatusCode(201);

        }

        [HttpPut]
        public async Task<IActionResult> UpdateEmployee([FromBody] Patient patient)
        {
            await _mediator.Send(new UpdatePatientCommand(patient));
            return StatusCode(201);

        }

        [HttpDelete]
        public async Task<IActionResult> DeleteEmployee([FromBody] int id)
        {
            await _mediator.Send(new DeletePatientCommand(id));
            return StatusCode(200);
        }
    
}
}
