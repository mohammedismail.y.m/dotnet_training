﻿using System;
using System.Collections.Generic;

namespace Patients2_CQRS_Example.Models;

public partial class Patient
{
    public string? PatientName { get; set; }

    public int PatientId { get; set; }
}
