﻿using Microsoft.EntityFrameworkCore;

namespace Code_First_Example.Models
{
    public class ProductDbContext:DbContext
    {

        public ProductDbContext(DbContextOptions<ProductDbContext> options):base(options)
        {

        }

        DbSet<Product> Products { get; set; }

        DbSet<User> Users { get; set; }

    }
}
