﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Code_First_Example.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        
        public string? Name { get; set; }
        
        public string? Email { get; set; }

        public string? Password { get; set; }

        public int? productId { get; set; }

        public Product product { get; set; }


     
    }
}
