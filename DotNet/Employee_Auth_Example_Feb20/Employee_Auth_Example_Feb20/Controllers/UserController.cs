﻿using Employee_Auth_Example_Feb20.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Employee_Auth_Example_Feb20.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        Employee_AuthContext _AuthContext = new Employee_AuthContext();
        IConfiguration _configuration;
        public UserController(IConfiguration configuration, Employee_AuthContext AuthContext)
        {
             
            _configuration = configuration;
            _AuthContext= AuthContext;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> loginDetails(UserDetail user)
        {
            if(user != null)
            {
                var check = await _AuthContext.UserDetails.Where(a => a.UserName == user.UserName && a.Password == user.Password).FirstOrDefaultAsync();

                if(check != null)
                {
                    user.UserTok = GetToken(user);
                    return Ok("Success");
                }
                else
                {
                    return BadRequest("BadRequest");
                }

            }
            else
            {
                return BadRequest("BadRequest");
            }
        }

        private string GetToken(UserDetail user)
        {
            var tokenClaims = new[]{
                new Claim(JwtRegisteredClaimNames.Sub, _configuration["Jwt:Subject"]),
                new Claim(JwtRegisteredClaimNames.Jti,Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
                new Claim("UserName", user.UserName.ToString())



            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var signin = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"], _configuration["Jwt:Audience"], tokenClaims, expires: DateTime.Now.AddMinutes(15), signingCredentials: signin);
            user.UserTok = new JwtSecurityTokenHandler().WriteToken(token);
            return user.UserTok.ToString();

        }
    }
}
