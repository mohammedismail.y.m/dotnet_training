﻿using Employee_Auth_Example_Feb20.Models;

namespace Employee_Auth_Example_Feb20.Interfaces
{
    public interface IUser
    {
        List<UserDetail> createNewUser(UserDetail user);
        List<UserDetail> GetAllUsers();

        string deleteUser(int id);

        List<UserDetail> updateUser(UserDetail user);
    }
}
