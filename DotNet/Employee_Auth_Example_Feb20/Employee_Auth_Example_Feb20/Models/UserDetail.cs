﻿using System;
using System.Collections.Generic;

namespace Employee_Auth_Example_Feb20.Models
{
    public partial class UserDetail
    {
        public int Id { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? UserTok { get; set; }
    }
}
