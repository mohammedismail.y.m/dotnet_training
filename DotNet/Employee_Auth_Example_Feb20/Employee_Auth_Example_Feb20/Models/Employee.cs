﻿using System;
using System.Collections.Generic;

namespace Employee_Auth_Example_Feb20.Models
{
    public partial class Employee
    {
        public int EmployeeId { get; set; }
        public string? EmployeeName { get; set; }
        public string? Designation { get; set; }
        public int? Salary { get; set; }
    }
}
