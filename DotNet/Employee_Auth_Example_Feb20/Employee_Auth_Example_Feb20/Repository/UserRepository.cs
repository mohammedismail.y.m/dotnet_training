﻿using Employee_Auth_Example_Feb20.Interfaces;
using Employee_Auth_Example_Feb20.Models;

namespace Employee_Auth_Example_Feb20.Repository
{
    public class UserRepository : IUser
    {
        private readonly Employee_AuthContext _context;

        public UserRepository(Employee_AuthContext context)
        {
            _context = context;
        }



        public List<UserDetail> createNewUser(UserDetail user)
        {
            _context.UserDetails.Add(user);
            _context.SaveChanges();
            return _context.UserDetails.ToList();
        }

        public string deleteUser(int id)
        {
            throw new NotImplementedException();
        }

        //public string deleteFurniture(int id)
        //{
        //    var fur = _context.Furnitures.Find(id);
        //    _context.Furnitures.Remove(fur);
        //    _context.SaveChanges();


        //    return "Deleted successfully";
        //}



        public List<UserDetail> GetAllUsers()
        {
            return _context.UserDetails.ToList();
        }

        //public List<Furniture> updateFurniture(Furniture furniture)
        //{
        //    var findFur = _context.Furnitures.Find(furniture.Id);
        //    if (findFur != null)
        //    {
        //        findFur.FurnitureName = furniture.FurnitureName;
        //        findFur.Cost = furniture.Cost;
        //        _context.SaveChanges();
        //        return _context.Furnitures.ToList();


        //    }

        //    return _context.Furnitures.ToList();


        //}


        public List<UserDetail> updateUser(UserDetail user)
        {
            throw new NotImplementedException();
        }
    }
}
