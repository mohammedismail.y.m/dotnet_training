﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MVC_CRUDexample.Models;

namespace MVC_CRUDexample.Controllers
{
    public class TemployeesController : Controller
    {
        private readonly EmployeeContext _context;

        public TemployeesController(EmployeeContext context)
        {
            _context = context;
        }

        // GET: Temployees
        public async Task<IActionResult> Index()
        {
              return View(await _context.Temployees.ToListAsync());
        }

        // GET: Temployees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Temployees == null)
            {
                return NotFound();
            }

            var temployee = await _context.Temployees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (temployee == null)
            {
                return NotFound();
            }

            return View(temployee);
        }

        // GET: Temployees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Temployees/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,EmpName,Address,Salary,Age")] Temployee temployee)
        {
            if (ModelState.IsValid)
            {
                _context.Add(temployee);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(temployee);
        }

        // GET: Temployees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Temployees == null)
            {
                return NotFound();
            }

            var temployee = await _context.Temployees.FindAsync(id);
            if (temployee == null)
            {
                return NotFound();
            }
            return View(temployee);
        }

        // POST: Temployees/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,EmpName,Address,Salary,Age")] Temployee temployee)
        {
            if (id != temployee.EmployeeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(temployee);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TemployeeExists(temployee.EmployeeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(temployee);
        }

        // GET: Temployees/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Temployees == null)
            {
                return NotFound();
            }

            var temployee = await _context.Temployees
                .FirstOrDefaultAsync(m => m.EmployeeId == id);
            if (temployee == null)
            {
                return NotFound();
            }

            return View(temployee);
        }

        // POST: Temployees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Temployees == null)
            {
                return Problem("Entity set 'EmployeeContext.Temployees'  is null.");
            }
            var temployee = await _context.Temployees.FindAsync(id);
            if (temployee != null)
            {
                _context.Temployees.Remove(temployee);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TemployeeExists(int id)
        {
          return _context.Temployees.Any(e => e.EmployeeId == id);
        }
    }
}
