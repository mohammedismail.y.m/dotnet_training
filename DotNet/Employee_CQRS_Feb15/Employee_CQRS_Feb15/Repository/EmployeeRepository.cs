﻿using Employee_CQRS_Feb15.DataAccess;
using Employee_CQRS_Feb15.Models;

namespace Employee_CQRS_Feb15.Repository
{
    public class EmployeeRepository:IEmployee
    {
        private readonly EmployeeContext _context;

        public EmployeeRepository(EmployeeContext context)
        {
            _context = context;
        }

        //Create new employee
        public List<Temployee> CreateNewEmployee(Temployee employee)
        {
            _context.Temployees.Add(employee);
            _context.SaveChanges();
            return _context.Temployees.ToList();
        }


        //delete employee
        public string DeleteEmployee(int id)
        {
            var getEmployeeId = _context.Temployees.Find(id);
            if(getEmployeeId != null) {
                _context.Temployees.Remove(getEmployeeId);
                _context.SaveChanges();
                return "deleted successfully";
            }
            else
            {
                return "Not found";
            }   
        }

        //get list of employees
        public List<Temployee> GetEmployeeList()
        {
           return  _context.Temployees.ToList();
        }

        //update employee
        public List<Temployee> updateEmployee(Temployee employee)
        {
            var getEmployeeId = _context.Temployees.Find(employee.EmployeeId);
            if(getEmployeeId != null)
            {
                getEmployeeId.EmpName = employee.EmpName;
                getEmployeeId.Salary = employee.Salary;
                getEmployeeId.Age= employee.Age;
                getEmployeeId.Address = employee.Address;
                _context.SaveChanges();
                return _context.Temployees.ToList();

            }
            else
            {
                return _context.Temployees.ToList();
            }

        }
    }
}
