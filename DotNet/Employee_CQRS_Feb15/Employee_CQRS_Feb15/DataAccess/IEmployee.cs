﻿using Employee_CQRS_Feb15.Models;

namespace Employee_CQRS_Feb15.DataAccess
{
    public interface IEmployee
    {
        //To get list of employees
        List<Temployee> GetEmployeeList();

        //Insert a new record
        public List<Temployee> CreateNewEmployee(Temployee employee);

        //update a record
        public List<Temployee> updateEmployee(Temployee employee);

        //delete a record
        public string DeleteEmployee(int id);


    }
}
