using Employee_CQRS_Feb15.DataAccess;
using Employee_CQRS_Feb15.Repository;
using MediatR;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<EmployeeContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddScoped<IEmployee, EmployeeRepository>();
//builder.Services.AddSingleton<IMediator, Mediator>();
builder.Services.AddMediatR(typeof(EmployeeRepository).Assembly);


//CORS
builder.Services.AddCors(options =>
{
    options.AddPolicy("angular", builde =>
    {
        builde.WithOrigins("http://localhost:53844").AllowAnyMethod().AllowAnyHeader();
    });
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{

    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseCors("angular");
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
