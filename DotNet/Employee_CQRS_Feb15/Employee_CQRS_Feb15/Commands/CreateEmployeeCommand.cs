﻿using Employee_CQRS_Feb15.Models;
using MediatR;

namespace Employee_CQRS_Feb15.Commands
{
    public record CreateEmployeeCommand(Temployee employee):IRequest<List<Temployee>>
    {

    }
}
