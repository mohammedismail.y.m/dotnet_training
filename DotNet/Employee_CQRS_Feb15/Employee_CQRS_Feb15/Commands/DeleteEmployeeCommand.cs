﻿
using MediatR;

namespace Employee_CQRS_Feb15.Commands
{
    public record DeleteEmployeeCommand(int id):IRequest<string>;
  
}
