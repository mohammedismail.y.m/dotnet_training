﻿using Employee_CQRS_Feb15.Models;
using MediatR;

namespace Employee_CQRS_Feb15.Commands
{
    public record UpdateEmployeeCommand(Temployee updateEmployee) : IRequest<List<Temployee>>
    {
    }
}
