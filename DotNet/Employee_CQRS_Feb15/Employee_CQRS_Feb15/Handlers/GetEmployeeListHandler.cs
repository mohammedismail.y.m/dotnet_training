﻿using Employee_CQRS_Feb15.DataAccess;
using Employee_CQRS_Feb15.Models;
using Employee_CQRS_Feb15.Queries;
using MediatR;

namespace Employee_CQRS_Feb15.Handlers
{
    public class GetEmployeeListHandler:IRequestHandler<GetEmployeeListQuery,List<Temployee>>
    {
        private readonly IEmployee _employee;

        public GetEmployeeListHandler(IEmployee employee) { 
            _employee = employee;
            
        }

        public async Task<List<Temployee>> Handle(GetEmployeeListQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.GetEmployeeList());
        }
    }
}
