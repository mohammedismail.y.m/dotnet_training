﻿using Employee_CQRS_Feb15.Commands;
using Employee_CQRS_Feb15.DataAccess;
using Employee_CQRS_Feb15.Models;
using MediatR;

namespace Employee_CQRS_Feb15.Handlers
{
    public class CreateEmployeeHandler : IRequestHandler<CreateEmployeeCommand, List<Temployee>>
    {
        private readonly IEmployee _employee;

        public CreateEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }
        public async Task<List<Temployee>> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {

            return await Task.FromResult(_employee.CreateNewEmployee(request.employee));
        }
    }
}
