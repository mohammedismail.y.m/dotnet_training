﻿using Employee_CQRS_Feb15.Commands;
using Employee_CQRS_Feb15.DataAccess;
using MediatR;

namespace Employee_CQRS_Feb15.Handlers
{
    public class DeleteEmployeeHandler : IRequestHandler<DeleteEmployeeCommand, string>
    {
        private readonly IEmployee _employee;

        public DeleteEmployeeHandler(IEmployee employee)
        {
            _employee = employee;
        }
        public async Task<string> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_employee.DeleteEmployee(request.id));
        }
    }
}
