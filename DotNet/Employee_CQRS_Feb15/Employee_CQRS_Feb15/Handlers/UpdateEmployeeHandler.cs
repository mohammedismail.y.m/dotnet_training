﻿using Employee_CQRS_Feb15.Commands;
using Employee_CQRS_Feb15.DataAccess;
using Employee_CQRS_Feb15.Models;
using MediatR;

namespace Employee_CQRS_Feb15.Handlers
{
    public class UpdateEmployeeHandler : IRequestHandler<UpdateEmployeeCommand, List<Temployee>>
    {
        private readonly IEmployee _employee;

        public UpdateEmployeeHandler(IEmployee employee)
        {
                _employee = employee;
        }
        public async Task<List<Temployee>> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {

            return await Task.FromResult(_employee.updateEmployee(request.updateEmployee));
        }
    }
}
