﻿using Employee_CQRS_Feb15.Commands;
using Employee_CQRS_Feb15.Models;
using Employee_CQRS_Feb15.Queries;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Employee_CQRS_Feb15.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("angular")]
    public class EmployeeController : ControllerBase
    {
        private readonly IMediator _mediator;

        public EmployeeController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<List<Temployee>> GetEmployeeList()
        {
            var employeeList = await _mediator.Send(new GetEmployeeListQuery());
            return  employeeList;
        }

        [HttpPost]
        [Route("AddEmployee")]
        public async Task<IActionResult> CreateEmployee([FromBody] Temployee employee)
        {
            await _mediator.Send(new CreateEmployeeCommand(employee));
            return StatusCode(201);

        }

        [HttpPut]
        [Route("EditEmployee")]
        public async Task<IActionResult> UpdateEmployee([FromBody] Temployee employee)
        {
            var getList= await _mediator.Send(new UpdateEmployeeCommand(employee));
            return StatusCode(201,getList);

        }

        [HttpDelete]
        [Route("DeleteEmployee/{id}")]
        public async Task<IActionResult> DeleteEmployee( int id)
        {
           var message= await _mediator.Send(new DeleteEmployeeCommand(id));
            //var getData= GetEmployeeList();
            return StatusCode(200,new { Message = message });
        }
    }
}
