﻿using Employee_CQRS_Feb15.Models;
using MediatR;

namespace Employee_CQRS_Feb15.Queries
{
    public record GetEmployeeListQuery:IRequest<List<Temployee>>
    {
    }
}
