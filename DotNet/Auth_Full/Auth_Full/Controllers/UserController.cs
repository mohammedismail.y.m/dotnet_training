﻿using Auth_Full.Context;
using Auth_Full.Helpers;
using Auth_Full.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Text.RegularExpressions;

namespace Auth_Full.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserDbContext _context;

        public UserController(UserDbContext context)
        {
            _context = context;
        }

        [HttpPost("authenticate")]
        public async Task<IActionResult> Authenticate([FromBody] User newUser)
        {
            if (newUser == null)
            {
                return BadRequest();
            }

            var user = await _context.Users.FirstOrDefaultAsync(x => x.Jsername == newUser.Jsername);
            if (user == null)
            {
                return NotFound(new { Message = "User not found" });
            }
            if (!PasswordHasher.VerifyPassword(newUser.Password,user.Password))
            {
                return BadRequest(new { Message = "Password is incorrect" });
            }

            return Ok(new { Message = "LoginSuccess" });
        }


        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody] User newUser)
        {
            if (newUser == null)
            {
                return BadRequest();
            }
            //check user
            if(await CheckUsernameExists(newUser.Jsername))
                return BadRequest(new {Message = "Username Already Exists"});

            //check email
            if (await CheckEmailExists(newUser.Email))
                return BadRequest(new { Message = "Email Already Exists" });

            //check password strength
            var checkStrengthOfPassword = CheckPasswordStrength(newUser.Password);
            if (!string.IsNullOrEmpty(checkStrengthOfPassword))
                return BadRequest(new {Message = checkStrengthOfPassword.ToString()});
            

            newUser.Password = PasswordHasher.HashPassword(newUser.Password);
            newUser.Role = "User";
            await _context.Users.AddAsync(newUser);
            await _context.SaveChangesAsync();
            return Ok(new {Message = "Register success"});

        }

        private async Task<bool> CheckUsernameExists(string username)
        {
            return await _context.Users.AnyAsync(x=>x.Jsername==username);
        }

        private async Task<bool> CheckEmailExists(string email)
        {
            return await _context.Users.AnyAsync(x => x.Email == email);
        }

        private string CheckPasswordStrength(string password)
        {
            StringBuilder sb = new StringBuilder();
            if(password.Length < 8) 
                sb.Append("Minimum password length must be 8" + Environment.NewLine );
            if(!(Regex.IsMatch(password,"[a-z]") && Regex.IsMatch(password,"[A-Z]") && Regex.IsMatch(password,"[0-9]")))
                sb.Append("Password must contain AlphaNumeric"+ Environment.NewLine);
            if(!Regex.IsMatch(password,"[<,>,@,!,#,$,%,^,&,*,(,),_,+,\\[,\\],{,},?,:,;,',',/]"))
                sb.Append("Password should contain special chars" + Environment.NewLine);

            return sb.ToString();

            

        }

    }
}
