﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem.Models;

public partial class BorrowLedger
{
    public int BorrowId { get; set; }

    public int? StudentId { get; set; }

    public int? BookId { get; set; }

    public string? TakenDate { get; set; }

    public string? ReturnDate { get; set; }

    public virtual Book? Book { get; set; }

    public virtual StudentInfo? Student { get; set; }
}
