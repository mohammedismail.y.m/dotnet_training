﻿using System;
using System.Collections.Generic;

namespace LibraryManagementSystem.Models;

public partial class Book
{
    public int BookId { get; set; }

    public string? Name { get; set; }

    public string? PageCount { get; set; }

    public int? AuthorId { get; set; }

    public int? GenreId { get; set; }

    public virtual Author? Author { get; set; }

    public virtual ICollection<BorrowLedger> BorrowLedgers { get; } = new List<BorrowLedger>();

    public virtual Genre? Genre { get; set; }
}
