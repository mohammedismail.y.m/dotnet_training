﻿using Microsoft.EntityFrameworkCore;
using WEBAPI_CRUDexample.Interface;
using WEBAPI_CRUDexample.Models;

namespace WEBAPI_CRUDexample.Repository
{
    public class EmployeeRepo : IEmployee
    {
        private readonly EmployeeContext _context;
        public EmployeeRepo(EmployeeContext context) {
            _context = context;
        
        }

        public List<Temployee> createNewEmployee(Temployee employee)
        {
            _context.Temployees.Add(employee);
            _context.SaveChanges();
            return _context.Temployees.ToList();
        }

        public string deleteEmployee(int id)
        {
            var emp = _context.Temployees.Find(id);
            _context.Temployees.Remove(emp);
            _context.SaveChanges();
            
            
            return "Deleted successfully";
        }

        public List<Temployee> GetAllEmployees()
        {
            return _context.Temployees.ToList();
        }

        public string getEmployeeNameById(int id)
        {
            var empName = _context.Temployees.Where(p => p.EmployeeId== id).Select(p=>p.EmpName).FirstOrDefault();
            return empName;
        }

        public List<Temployee> updateEmployee(Temployee employee)
        {
            var findEmp = _context.Temployees.Find(employee.EmployeeId);
            findEmp.EmpName = employee.EmpName;
            findEmp.Salary = employee.Salary;
            findEmp.Address = employee.Address;
            findEmp.Age = employee.Age;

            
            _context.SaveChanges();

            return _context.Temployees.ToList() ;
        }

        /*private readonly EmployeeContext empRepo;
        private DbSet<Temployee> employeeService;

        public EmployeeRepo(EmployeeContext empRepo)
        {
            this.empRepo = empRepo;
            employeeService = empRepo.Set<Temployee>();

        }

        public List<Temployee> createNewEmployee(Temployee employee)
        {
            return empRepo.AddEmployee(employee);
        }

        IEnumerable<Temployee> IEmployee.GetAllEmployees()
        {
            return employeeService.AsEnumerable();
       }*/


        /*public List<Temployee> GetAllEmployees()
        {
            return empRepo.Temployees.ToList();
        }*/

    }
}
