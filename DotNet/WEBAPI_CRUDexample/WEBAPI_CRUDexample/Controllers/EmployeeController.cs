﻿using Microsoft.AspNetCore.Mvc;
using WEBAPI_CRUDexample.Interface;
using WEBAPI_CRUDexample.Models;
using WEBAPI_CRUDexample.Repository;

namespace WEBAPI_CRUDexample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EmployeeController : Controller
    {
        private readonly IEmployee repo;

        public EmployeeController(IEmployee repo)
        {
            this.repo = repo;
        }

        //Get employee details
        [HttpGet]
        public List<Temployee> GetEmp()
        {
            return repo.GetAllEmployees();
        }



        //Get employee name by id
        [HttpGet("{id}")]
        public string GetEmployeeName(int id)
        {
            return repo.getEmployeeNameById(id);
        }

        //Add Employee
        [HttpPost]
        public List<Temployee> NewEmp(Temployee emp)
        {
            return (repo.createNewEmployee(emp));
        }


        //delete employee
        [HttpDelete("{EmployeeId}")]
        public string DeleteEmp(int EmployeeId)
        {
           var msg= repo.deleteEmployee(EmployeeId);
            return msg;

        }


        //update employee
        [HttpPut]
        public List<Temployee> updateEmp(Temployee Employee)
        {
            return repo.updateEmployee(Employee);

        }
            
        






    }
}
