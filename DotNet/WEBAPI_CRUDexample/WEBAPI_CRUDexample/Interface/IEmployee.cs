﻿using WEBAPI_CRUDexample.Models;

namespace WEBAPI_CRUDexample.Interface
{
    public interface IEmployee
    {
        /* define methods*/
        List<Temployee> createNewEmployee(Temployee employee);
        List<Temployee> GetAllEmployees();

        String deleteEmployee(int id);

        List<Temployee> updateEmployee(Temployee employee);

        string getEmployeeNameById(int id);
        

        



    }
}
