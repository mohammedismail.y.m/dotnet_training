﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Runtime.CompilerServices;

namespace WEBAPI_CRUDexample.Models
{
    public partial class Temployee
    {
        [Required]
        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "Please provide your name")]
        
        public string? EmpName { get; set; }


        public string? Address { get; set; }
        public int? Salary { get; set; }
        public int? Age { get; set; }
    }
}
