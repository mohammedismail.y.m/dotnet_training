using Moq;
using WEBAPI_CRUDexample.Controllers;
using WEBAPI_CRUDexample.Interface;

namespace WebAPITests_Feb17
{
    public class WebApiTest
    {
        //add mock as handling all functionalities
        public Mock<IEmployee> mock = new Mock<IEmployee>();
        [Fact]
        public void Test1()
        {

        }

        [Fact]
        public void GetEmployeeNameById()
        {
            //arrange
            mock.Setup(p => p.getEmployeeNameById(1)).Returns("Mohammed");
            EmployeeController emp = new EmployeeController(mock.Object);




            //act
            var result = emp.GetEmployeeName(1);


            //assert
            Assert.Equal(result, "Mohammed");
        }

        [Fact]
        public void AddNewEmployee()
        {
            //arrange
            mock.Setup(p => p.createNewEmployee()).Returns();
            EmployeeController emp = new EmployeeController(mock.Object);




            //act
            var result = emp.GetEmployeeName(1);


            //assert
            Assert.Equal(result, "Mohammed");

        }
    }
}