﻿using Customers.Commands;
using Customers.DataAccess;
using Customers.Models;
using MediatR;

namespace Customers.Handler
{
    public class AddNewUserHandler : IRequestHandler<AddNewUserCommand, List<User>>
    {
        private readonly IUser _user;

        public AddNewUserHandler(IUser user)
        {
            _user= user;
        }
        public async Task<List<User>> Handle(AddNewUserCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_user.createNewUser(request.newUser));
        }
    }
}
