﻿using Customers.DataAccess;
using Customers.Models;
using Customers.Queries;
using MediatR;

namespace Customers.Handler
{
    public class getAllUsersHandler : IRequestHandler<getAllCustomersQuery, List<User>>
    {
        private readonly IUser _user;

        public getAllUsersHandler(IUser user)
        {
            _user = user;

        }
        public async Task<List<User>> Handle(getAllCustomersQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_user.getAllUsers());
        }
    }
}
