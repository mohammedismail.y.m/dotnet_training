﻿using Customers.Models;
using MediatR;

namespace Customers.Queries
{
    public record getAllCustomersQuery:IRequest<List<User>>
    {
        
    }
}
