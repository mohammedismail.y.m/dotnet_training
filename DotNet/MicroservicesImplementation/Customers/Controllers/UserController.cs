﻿using MediatR;
using Customers.Models;
using Customers.Queries;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Customers.Commands;

namespace Customers.Controllers
{
    [EnableCors("customer")]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IMediator _mediator;


        public UserController(IMediator mediator)
        {
            _mediator = mediator; 
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers()
        {
            var userList = _mediator.Send(new getAllCustomersQuery());
            return Ok(userList);
        }

        [HttpPost]
        public async Task<IActionResult> AddNewUser([FromBody] User newUser)
        {
            await _mediator.Send(new AddNewUserCommand(newUser));
            return StatusCode(201);
        }
        //public List<User> GetAllUsers()
        //{
        //    return _user.getAllUsers();
        //}

        //[HttpPost]
        //public List<User> NewUser(User newUser)
        //{
        //    return (_user.createNewUser(newUser));
        //}

        //[HttpDelete]
        //public string DeleteUser(int userId)
        //{
        //    var msg = _user.deleteUser(userId);
        //    return msg;

        //}

        //[HttpPut]
        //public string updateUser(User updateUser)
        //{
        //    var msg = _user.updateUser(updateUser);
        //    return msg;


        //}


    }
}
