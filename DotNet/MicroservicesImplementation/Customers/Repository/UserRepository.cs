﻿using Customers.DataAccess;
using Customers.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations.Operations;

namespace Customers.Repository
{
    public class NotFoundKeyException : Exception
    {
        public NotFoundKeyException(string message) : base(message)
        {
            //Console.WriteLine("This record is associated with another entity");
        }
    }
    public class UserRepository : IUser
    {
        private readonly UserDbContext _dbContext;

        public UserRepository(UserDbContext context)
        {
            _dbContext = context;
        }
        public List<User> createNewUser(User newUser)
        {
            _dbContext.Users.Add(newUser);
            _dbContext.SaveChanges();
            return _dbContext.Users.ToList();
        }

        public string deleteUser(int id)
        {
            var getUserId = _dbContext.Users.Find(id);
            if (getUserId != null)
            {
                _dbContext.Users.Remove(getUserId);
                _dbContext.SaveChanges();
                return "deleted successfully";
            }
            throw new NotFoundKeyException("NOT FOUND!");
        }

        public List<User> getAllUsers()
        {
            return _dbContext.Users.ToList();
        }

        public string updateUser(User updateUser)
        {
            var getUserId = _dbContext.Users.Find(updateUser.UserId);
            if (getUserId != null)
            {
                getUserId.UserName = updateUser.UserName;
                getUserId.UserPhone = updateUser.UserPhone;
                getUserId.UserAddress = updateUser.UserAddress;
                getUserId.UserEmail = updateUser.UserEmail;
                getUserId.UserPassword = updateUser.UserPassword;
                getUserId.UserRole = updateUser.UserRole;

                _dbContext.SaveChanges();
                return "UpdatedSuccessfully";

            }
            else
            {
                return "Not found";
            }
        }
    }
}
