﻿using Customers.Models;
using MediatR;

namespace Customers.Commands
{
    public record AddNewUserCommand(User newUser):IRequest<List<User>>
    {
    }
}
