﻿using Customers.Models;

namespace Customers.DataAccess
{
    public interface IUser
    {
        List<User> getAllUsers();

        public List<User> createNewUser(User newUser);

        public string updateUser(User updateUser);

        public string deleteUser(int id);




    }
}
