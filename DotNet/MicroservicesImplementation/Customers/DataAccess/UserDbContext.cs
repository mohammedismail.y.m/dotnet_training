﻿using Customers.Models;
using Microsoft.EntityFrameworkCore;

namespace Customers.DataAccess
{
    public class UserDbContext:DbContext
    {

        public UserDbContext(DbContextOptions<UserDbContext> options):base(options) { }
        
        public DbSet<User> Users { get; set; }
    }
}
