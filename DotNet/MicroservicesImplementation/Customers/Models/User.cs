﻿using System.ComponentModel.DataAnnotations;

namespace Customers.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string UserEmail { get; set; }

        public string UserPhone { get; set; }

        public string UserPassword { get; set; } = string.Empty;

        public string UserAddress { get; set; }

        public string UserRole { get; set; }
    }
}
