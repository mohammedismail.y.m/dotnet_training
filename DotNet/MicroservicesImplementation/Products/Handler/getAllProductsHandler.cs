﻿using MediatR;
using Products.DataAccess;
using Products.Model;
using Products.Query;

namespace Products.Handler
{
    public class getAllProductsHandler : IRequestHandler<getAllProductsQuery, List<Product>>
    {
        private readonly IProduct _product;

        public getAllProductsHandler(IProduct product)
        {
            _product = product;
        }

        public Task<List<Product>> Handle(getAllProductsQuery request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_product.getAllProducts());
        }
    }
}
