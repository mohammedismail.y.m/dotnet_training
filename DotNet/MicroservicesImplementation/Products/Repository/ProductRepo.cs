﻿using Products.DataAccess;
using Products.Model;

namespace Products.Repository
{
    public class ProductRepo: IProduct
    {
        private readonly ProductDbContext _dbContext;
        public ProductRepo(ProductDbContext dbContext) 
        {

            _dbContext = dbContext;
        
        
        }

        public List<Product> getAllProducts()
        {
            return _dbContext.Products.ToList();
        }
    }
}
