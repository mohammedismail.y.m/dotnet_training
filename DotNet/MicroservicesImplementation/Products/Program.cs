using MediatR;
using Microsoft.EntityFrameworkCore;
using Products.DataAccess;
using Products.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<ProductDbContext>(option => option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConn")));
builder.Services.AddScoped<IProduct, ProductRepo>();
builder.Services.AddMediatR(typeof(ProductRepo).Assembly);
//CORS implementation
builder.Services.AddCors(options =>
{
    options.AddPolicy("product", builde =>
    {
        builde.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
    });
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseCors("product");
app.UseAuthorization();

app.MapControllers();

app.Run();
