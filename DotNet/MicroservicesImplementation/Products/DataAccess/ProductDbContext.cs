﻿using Microsoft.EntityFrameworkCore;
using Products.Model;

namespace Products.DataAccess
{
    public class ProductDbContext:DbContext
    {
        public ProductDbContext(DbContextOptions<ProductDbContext> options):base(options) { }
        
        public DbSet<Product> Products { get; set; }
    }
}
