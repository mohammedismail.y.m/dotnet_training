﻿using Products.Model;

namespace Products.DataAccess
{
    public interface IProduct
    {
        List<Product> getAllProducts();

    }
}
