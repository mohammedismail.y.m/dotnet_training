﻿using System.ComponentModel.DataAnnotations;

namespace Products.Model
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductDescription { get; set; }

        public int ProductCost { get; set; }

    }
}
