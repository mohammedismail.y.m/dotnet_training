﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Payments.Model;
using Payments.Services;

namespace Payments.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentsController : ControllerBase
    {
        private readonly PaymentsService _paymentService;

        public PaymentsController(PaymentsService paymentService)
        {
            _paymentService = paymentService;
        }

        [HttpGet]
        public async Task<List<Payment>> Get() =>
        await _paymentService.GetAsync();

    }
}
