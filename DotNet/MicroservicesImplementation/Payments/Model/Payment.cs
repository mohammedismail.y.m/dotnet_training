﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Payments.Model
{
    public class Payment
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }

        [BsonElement("userid")]
        public int? userid { get; set; }


        public int? orderid { get; set; }


        public int? productid { get; set; }


        public string? firstname { get; set; }

        public string? lastname { get; set; }

        public int? amountpaid { get; set; }

        public string? paymenttype { get; set; }

        public string? city { get; set; }

        public int? PIN { get; set; }





    }
}
