﻿namespace Payments.Model
{
    public class PaymentsDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

        public string PaymentsCollectionName { get; set; } = null!;
    }
}
