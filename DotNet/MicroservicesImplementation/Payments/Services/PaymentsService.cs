﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Payments.Model;

namespace Payments.Services
{
    public class PaymentsService
    {
        private readonly IMongoCollection<Payment> _payment;
        public PaymentsService(IOptions<PaymentsDatabaseSettings> paymentsDatabaseSettings)
        {

            var mongoClient = new MongoClient(
            paymentsDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                paymentsDatabaseSettings.Value.DatabaseName);

            _payment = mongoDatabase.GetCollection<Payment>(
                paymentsDatabaseSettings.Value.PaymentsCollectionName);

        }

        public async Task<List<Payment>> GetAsync() =>
       await _payment.Find(_ => true).ToListAsync();

    }
}
