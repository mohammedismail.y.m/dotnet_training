﻿using MediatR;

namespace Movie_CQRS_Example.Commands
{
    public record DeleteMovieCommand(int id):IRequest<string>
    {
    }
}
