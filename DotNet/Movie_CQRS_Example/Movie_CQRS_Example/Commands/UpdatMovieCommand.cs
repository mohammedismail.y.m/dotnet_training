﻿using MediatR;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Commands
{
    public record UpdatMovieCommand(Movie updateMovie):IRequest<List<Movie>>
    {
    }
}
