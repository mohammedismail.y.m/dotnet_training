﻿using System;
using System.Collections.Generic;

namespace Movie_CQRS_Example.Models;

public partial class Movie
{
    public int MovieId { get; set; }

    public string? MovieName { get; set; }

    public int? GenreId { get; set; }

    public virtual Genre? Genre { get; set; }

    public static implicit operator List<object>(Movie v)
    {
        throw new NotImplementedException();
    }
}
