﻿using System;
using System.Collections.Generic;

namespace Movie_CQRS_Example.Models;

public partial class Genre
{
    public int GenreId { get; set; }

    public string? GenreName { get; set; }

    public virtual ICollection<Movie> Movies { get; } = new List<Movie>();
}
