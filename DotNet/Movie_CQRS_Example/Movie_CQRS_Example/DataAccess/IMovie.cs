﻿using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.DataAccess
{
    public interface IMovie
    {

        //To get list of employees
        List<Movie> GetMovieList();

        //Insert a new record
        public List<Movie> CreateNewMovie(Movie movie);

        //update a record
        public List<Movie> updateMovie(Movie movie);

        //delete a record
        public string DeleteMovie(int id);

        List<Movie> GetMoviesBasedOnGenres(int id);

        //public Task<Movie> GetMovieById(int id);

        //public Task<string> GetMovieName(int id);
    }
}
