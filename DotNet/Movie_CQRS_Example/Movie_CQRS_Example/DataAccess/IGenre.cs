﻿using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.DataAccess
{
    public interface IGenre
    {
        List<Genre> GetGenreList();
    }
}
