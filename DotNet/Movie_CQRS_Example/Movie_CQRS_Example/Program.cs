using MediatR;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.EntityFrameworkCore;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Repository;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<MoviesContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddScoped<IMovie, MovieRepository>();
builder.Services.AddScoped<IGenre, GenreRepository>();
//builder.Services.AddSingleton<IMediator, Mediator>();
builder.Services.AddMediatR(typeof(MovieRepository).Assembly);
builder.Services.AddMediatR(typeof(GenreRepository).Assembly);

//Add Service for JSON-Serializer
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    options.JsonSerializerOptions.WriteIndented = true;
});

//CORS implementation
builder.Services.AddCors(options =>
{
    options.AddPolicy("swagger", builde =>
    {
        builde.WithOrigins("https://localhost:7108/").AllowAnyOrigin().AllowAnyMethod();
    });
});

//logging
builder.Services.AddHttpLogging(logging =>
{
    logging.LoggingFields = HttpLoggingFields.All;
    
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
    app.UseHttpLogging(); // added for logging
}

app.UseHttpsRedirection();

app.UseCors("swagger");
app.UseAuthorization();

app.MapControllers();

app.Run();
