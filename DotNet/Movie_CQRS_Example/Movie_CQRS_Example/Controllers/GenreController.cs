﻿using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_CQRS_Example.Queries;

namespace Movie_CQRS_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class GenreController : ControllerBase
    {
        private readonly IMediator _mediator;

        public GenreController(IMediator mediator)
        {
            _mediator = mediator;
        }



        [HttpGet]
        public async Task<IActionResult> GetGenreList()
        {
            var genreList = _mediator.Send(new GetGenreListQuery());
            return Ok(genreList);
        }
    }
}
    