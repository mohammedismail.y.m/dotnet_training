﻿using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie_CQRS_Example.Commands;
using Movie_CQRS_Example.Models;
using Movie_CQRS_Example.Queries;

namespace Movie_CQRS_Example.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("angular")]
    public class MovieController : ControllerBase
    {
        private readonly IMediator _mediator;

        public MovieController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> GetMovieList()
        {
            var employeeList = _mediator.Send(new GetMovieListQuery());
            return Ok(employeeList);
        }

       

        [HttpGet("/BasedOnGenre/{id}")]
        public async Task<IActionResult> GetMovieBasedOnGenreList(int id)
        {
            var movieBasedOnGenreList = _mediator.Send(new GetMovieBasedOnGenre(id));
            return Ok(movieBasedOnGenreList);
        }

        

        [HttpPost]
        public async Task<IActionResult> CreateNewMovie([FromBody] Movie newMovie)
        {
            await _mediator.Send(new AddMovieCommand(newMovie));
            return StatusCode(201);

        }

        [HttpPut]
        public async Task<IActionResult> UpdateMovie([FromBody] Movie updateMovie)
        {
            await _mediator.Send(new UpdatMovieCommand(updateMovie));
            return StatusCode(201);

        }

        [HttpDelete]
        [Route("{id}")]
        public async Task<IActionResult> DeleteMovie([FromBody] int id)
        {
            await _mediator.Send(new DeleteMovieCommand(id));
            return StatusCode(200);
        }
    }
}
