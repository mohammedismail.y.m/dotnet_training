﻿using Microsoft.EntityFrameworkCore;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Repository
{
    public class GenreRepository : IGenre
    {
        private readonly MoviesContext _context;

        public GenreRepository(MoviesContext context)
        {
            _context = context;
        }

        public List<Genre> GetGenreList()
        {
            return _context.Genres.ToList();
        }
    }
}
