﻿using Microsoft.EntityFrameworkCore;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Repository
{
    public class NotFoundKeyException : Exception
    {
        public NotFoundKeyException(string message) : base(message)
        {
            //Console.WriteLine("This record is associated with another entity");
        }
    }
    public class MovieRepository : IMovie
    {
        private readonly MoviesContext _context;
        public MovieRepository(MoviesContext context)
        {
            _context = context;
        }

        public List<Movie> CreateNewMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            _context.SaveChanges();
            return _context.Movies.ToList();
        }

        public string DeleteMovie(int id)
        {

            var getMovieId = _context.Movies.Find(id);
            if (getMovieId != null)
            {
                _context.Movies.Remove(getMovieId);
                _context.SaveChanges();
                return "deleted successfully";
            }
            throw new NotFoundKeyException("NOT FOUND!");

        }

        //public async  Task<Movie> GetMovieById(int id)
        //{
        //    var movieById = await _context.Movies.Where(m => m.MovieId == id).FirstOrDefaultAsync();
        //    return  movieById;
        //}

        public List<Movie> GetMovieList()
        {
            return _context.Movies.Include(nameof(Genre)).ToList();
        }

        //public async Task<string> GetMovieName(int id)
        //{
        //    var name = await _context.Movies.Where(m => m.MovieId == id).Select(t => t.MovieName).FirstOrDefaultAsync();
        //    return name;
        //}

        public List<Movie> GetMoviesBasedOnGenres(int id)
        {
            List<Movie> searchBasedOnGenre = new List<Movie>();
            List<Movie> movieList = new List<Movie>();
            searchBasedOnGenre = _context.Movies.Where(x => x.GenreId == id).ToList();
            /*            foreach (var movie in movieList)
                        {
                            if (movie.GenreId == id)
                            {
                                searchBasedOnGenre.Add(movie);
                            }

                        }*/
            return searchBasedOnGenre;



        }

        public List<Movie> updateMovie(Movie movie)
        {
            var getMovieId = _context.Movies.Find(movie.MovieId);
            if (getMovieId != null)
            {
                getMovieId.MovieName = movie.MovieName;
                getMovieId.GenreId = movie.GenreId;

                _context.SaveChanges();
                return _context.Movies.ToList();

            }
            else
            {
                return _context.Movies.ToList();
            }
        }
    }
}
