﻿using MediatR;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Queries
{
    public record GetGenreListQuery:IRequest<List<Genre>>
    {
    }
}
