﻿using MediatR;
using Movie_CQRS_Example.Commands;
using Movie_CQRS_Example.DataAccess;

namespace Movie_CQRS_Example.Handler
{
    public class DeleteMovieHandler : IRequestHandler<DeleteMovieCommand, string>
    {
        private readonly IMovie _movie;

        public DeleteMovieHandler(IMovie movie)
        {
            _movie = movie;
        }
        public async Task<string> Handle(DeleteMovieCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movie.DeleteMovie(request.id));
        }
    }
}
