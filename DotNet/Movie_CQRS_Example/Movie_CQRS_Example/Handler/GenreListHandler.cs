﻿using MediatR;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;
using Movie_CQRS_Example.Queries;

namespace Movie_CQRS_Example.Handler
{
    public class GenreListHandler : IRequestHandler<GetGenreListQuery, List<Genre>>
    {
        private readonly IGenre _genre;

        public GenreListHandler(IGenre genre)
        {
            _genre = genre;
        }
        public async Task<List<Genre>> Handle(GetGenreListQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_genre.GetGenreList());
        }
    }
}
