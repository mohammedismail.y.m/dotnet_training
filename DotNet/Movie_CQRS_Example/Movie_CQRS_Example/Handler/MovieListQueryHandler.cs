﻿using MediatR;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;
using Movie_CQRS_Example.Queries;

namespace Movie_CQRS_Example.Handler
{
    public class MovieListQueryHandler : IRequestHandler<GetMovieListQuery, List<Movie>>
    {
        private readonly IMovie _movie;

        public MovieListQueryHandler(IMovie movie)
        {
            _movie = movie;
        }
        public async Task<List<Movie>> Handle(GetMovieListQuery request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movie.GetMovieList());
        }
    }
}
