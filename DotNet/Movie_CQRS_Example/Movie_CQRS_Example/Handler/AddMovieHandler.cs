﻿using MediatR;
using Movie_CQRS_Example.Commands;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Handler
{
    public class AddMovieHandler:IRequestHandler<AddMovieCommand, List<Movie>>
    {
        private readonly IMovie _movie;

        public AddMovieHandler(IMovie movie)
        {
            _movie = movie;
        }

        public Task<List<Movie>> Handle(AddMovieCommand request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_movie.CreateNewMovie(request.newMovie));
        }
    }
}
