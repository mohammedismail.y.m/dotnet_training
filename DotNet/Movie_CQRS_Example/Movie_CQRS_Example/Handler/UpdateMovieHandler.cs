﻿using MediatR;
using Movie_CQRS_Example.Commands;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;

namespace Movie_CQRS_Example.Handler
{
    public class UpdateMovieHandler : IRequestHandler<UpdatMovieCommand, List<Movie>>
    {
        private readonly IMovie _movie;

        public UpdateMovieHandler(IMovie movie)
        {
            _movie = movie;
        }
        public async Task<List<Movie>> Handle(UpdatMovieCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movie.updateMovie(request.updateMovie));
        }
    }
}
