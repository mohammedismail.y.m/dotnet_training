﻿using MediatR;
using Movie_CQRS_Example.DataAccess;
using Movie_CQRS_Example.Models;
using Movie_CQRS_Example.Queries;

namespace Movie_CQRS_Example.Handler
{
    public class GetMovieBasedOnGenreHandler : IRequestHandler<GetMovieBasedOnGenre, List<Movie>>
    {
        private readonly IMovie _movie;

        public GetMovieBasedOnGenreHandler(IMovie movie)
        {
            _movie = movie;
        }

        public async Task<List<Movie>> Handle(GetMovieBasedOnGenre request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_movie.GetMoviesBasedOnGenres(request.id));
            
        }
    }
}
