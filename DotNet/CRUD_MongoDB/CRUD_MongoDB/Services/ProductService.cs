﻿using CRUD_MongoDB.Model;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace CRUD_MongoDB.Services
{
    public class ProductService
    {
        private readonly IMongoCollection<Product> _product;
        public ProductService(IOptions<ProductsDatabaseSettings> productsDatabaseSettings)
        {

            var mongoClient = new MongoClient(
            productsDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                productsDatabaseSettings.Value.DatabaseName);

            _product = mongoDatabase.GetCollection<Product>(
                productsDatabaseSettings.Value.ProductsCollectionName);

        }

        public async Task<List<Product>> GetAsync() =>
       await _product.Find(_ => true).ToListAsync();


        public async Task<Product?> GetAsync(string id) =>
        await _product.Find(x => x.Id == id).FirstOrDefaultAsync();

        public async Task CreateAsync(Product newProduct) =>
       await _product.InsertOneAsync(newProduct);

        public async Task UpdateAsync(string id, Product updatedProduct) =>
            await _product.ReplaceOneAsync(x => x.Id == id, updatedProduct);

        public async Task RemoveAsync(string id) =>
            await _product.DeleteOneAsync(x => x.Id == id);
        
    }
}
